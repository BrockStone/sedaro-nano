# Sedaro-Nano App

![Application screenshot](./src/Assets/sedaro-nano-dash-shot.png)

This project was created to demonstate an app Dashboard build out given Planet and Satellite flight pattern data.

As a user of Sedaro-Nano you have the ability to play / scrub through a satellite flight pattern timeline and view corresponding flight data.

## Getting Started with the Sedaro-Nano App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

### Node Version 16.20 required to run the project

Use your node version manager of choice to use version 16.20:

### `nvm install 16.20`

### `nvm use 16.20`

In the project directory, run the following to start the project:

### `yarn && yarn start`

## Project Notes

If given more time / had additional knowledge of space travel this project would have included the following:

- Display corresponding details based on currently velocity / speed of Satellite or Planet
- 3D chart displaying flight patterns
- Increased performance of data flow to components
