import React from 'react';
import './App.css';
import { SedaroDashboardProvider } from './Contexts/SedaroDashboard';
import Dashboard from './Components/Dashboard';

function App() {
  return (
    <div className="App">
      <SedaroDashboardProvider>
        <Dashboard />
      </SedaroDashboardProvider>
    </div>
  );
}

export default App;
