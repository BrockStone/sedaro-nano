/* eslint-disable */
import React from "react";
import Plotly from "plotly.js";
import { PropTypes } from "prop-types";
import { data } from "../data";

// Create Context
const SedaroDashboardContext = React.createContext(undefined);

// Actions
export const SET_DASHBOARD_DATA_SETS = "set-dashboard-data-sets";
export const SET_DASHBOARD_ACTIVE_TIMESTAMP =
  "set-dashboard-data-active-timestamp";
export const SET_DASHBOARD_SAT_FLIGHT_SUMMARY =
  "set-dashboard-sat-flight-summary";
export const SET_DASHBOARD_PLANET_FLIGHT_SUMMARY =
  "set-dashboard-planet-flight-summary";
export const TOGGLE_PLAYER = "toggle-player";

const init = () => {
  return {
    flyingObjectsPlotData: undefined,
    flyingObjectVxSummaryPlotData: undefined,
    flyingObjectTimeframes: undefined,
    satelliteFlightSummary: undefined,
    planetFlightSummary: undefined,
    activeTimestamp: undefined,
    isPlaying: false
  };
};

// Reducer
const reducer = (state, action) => {
  switch (action.type) {
    case SET_DASHBOARD_DATA_SETS: {
      return {
        ...state,
        flyingObjectsPlotData: action.flyingObjectsPlotData,
        flyingObjectVxSummaryPlotData: action.flyingObjectVxSummaryPlotData,
        flyingObjectsFlightPatternLookup:
          action.flyingObjectsFlightPatternLookup,
        flyingObjectTimeframes: action.flyingObjectTimeframes
      };
    }
    case SET_DASHBOARD_SAT_FLIGHT_SUMMARY: {
      return {
        ...state,
        satelliteFlightSummary: action.satelliteFlightSummary
      };
    }
    case SET_DASHBOARD_PLANET_FLIGHT_SUMMARY: {
      return {
        ...state,
        planetFlightSummary: action.planetFlightSummary
      };
    }
    case SET_DASHBOARD_ACTIVE_TIMESTAMP: {
      return {
        ...state,
        activeTimestamp: action.activeTimestamp
      };
    }
    case TOGGLE_PLAYER: {
      return {
        ...state,
        isPlaying: !state.isPlaying
      };
    }
    default: {
      throw new Error(
        `Unhandled SedaroDashboardContext action type: ${action.type}`
      );
    }
  }
};

const SedaroDashboardProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, null, init);

  const formatDashboardDataSets = dataSet => {
    let flyingObjectsPlotData = {};
    let flyingObjectVxSummaryPlotData = {};
    for (let [t0, t1, frame] of dataSet) {
      for (let [agentId, { x, y, vx, vy, time }] of Object.entries(frame)) {
        if (state.activeTimestamp && time <= state.activeTimestamp) {
          flyingObjectsPlotData[agentId] = flyingObjectsPlotData[agentId] || {
            x: [],
            y: []
          };
          flyingObjectsPlotData[agentId].x.push(x);
          flyingObjectsPlotData[agentId].y.push(y);
          flyingObjectsPlotData[agentId].type = "line";
          flyingObjectsPlotData[agentId].mode = "markers";
          flyingObjectsPlotData[agentId].name = agentId;
          flyingObjectsPlotData[agentId].line = {
            color: agentId === "Planet" ? "grey" : "#1976d2"
          };

          flyingObjectVxSummaryPlotData[
            agentId
          ] = flyingObjectVxSummaryPlotData[agentId] || {
            x: [],
            y: []
          };
          flyingObjectVxSummaryPlotData[agentId].x.push(vx);
          flyingObjectVxSummaryPlotData[agentId].y.push(time);
          flyingObjectVxSummaryPlotData[agentId].type = "scatter";
          flyingObjectVxSummaryPlotData[agentId].mode = "markers";
          flyingObjectVxSummaryPlotData[agentId].name = agentId;
          flyingObjectVxSummaryPlotData[agentId].line = {
            color: agentId === "Planet" ? "grey" : "#1976d2"
          };
        }
      }
    }
    flyingObjectsPlotData = Object.values(flyingObjectsPlotData);
    flyingObjectVxSummaryPlotData = Object.values(
      flyingObjectVxSummaryPlotData
    );
    return { flyingObjectsPlotData, flyingObjectVxSummaryPlotData };
  };

  const generateFlightPatterTimeframes = dataSet => {
    var flyingObjectsFlightPatternLookup = {};

    // Get flying object data
    const getFlyingObjectData = (timestamp, flyingObject) => {
      var byTimeStamp, trace;
      if (!(byTimeStamp = flyingObjectsFlightPatternLookup[timestamp])) {
        byTimeStamp = flyingObjectsFlightPatternLookup[timestamp] = {};
      }

      // If a container for this timestamp + flyingObject doesn't exist yet, then create one:
      if (!(trace = byTimeStamp[flyingObject])) {
        trace = byTimeStamp[flyingObject] = {
          x: [],
          y: [],
          vx: [],
          vy: [],
          id: [],
          text: []
        };
      }
      return trace;
    };

    // Go through each row, get the right trace, and append the dataSet:
    for (let [t0, t1, frame] of dataSet) {
      for (let [agentId, { x, y, vx, vy, time }] of Object.entries(frame)) {
        var trace = getFlyingObjectData(time, agentId);
        trace.text.push(agentId);
        trace.id.push(agentId);
        trace.x.push(x);
        trace.y.push(y);
        trace.vx.push(vx);
        trace.vy.push(vy);
      }
    }

    // Get the group names (Planet / Satellite) and sort
    var flyingObjectTimeframes = Object.keys(flyingObjectsFlightPatternLookup);
    flyingObjectTimeframes = flyingObjectTimeframes.sort(
      (a, b) => Number(a) - Number(b)
    );

    const dataSets = {
      flyingObjectsFlightPatternLookup,
      flyingObjectTimeframes
    };

    return dataSets;
  };

  const generateDashboardDataSets = async () => {
    if (data && data.length > 0) {
      let frameData = data;

      // Format data sets
      const dashboardDataSets = formatDashboardDataSets(frameData);
      const flightPatternDataSets = generateFlightPatterTimeframes(frameData);

      const {
        flyingObjectsPlotData,
        flyingObjectVxSummaryPlotData
      } = dashboardDataSets;

      const {
        flyingObjectsFlightPatternLookup,
        flyingObjectTimeframes
      } = flightPatternDataSets;

      if (!state.activeTimestamp) {
        dispatch({
          type: SET_DASHBOARD_ACTIVE_TIMESTAMP,
          activeTimestamp: flyingObjectTimeframes[0]
        });
      }

      dispatch({
        type: SET_DASHBOARD_DATA_SETS,
        flyingObjectsPlotData,
        flyingObjectVxSummaryPlotData,
        flyingObjectsFlightPatternLookup,
        flyingObjectTimeframes
      });
    }
  };

  const togglePlayer = () => {
    dispatch({
      type: TOGGLE_PLAYER
    });
  };

  const setTimeframe = ({ target }) => {
    const timeframeSelected = target.value.toString();

    // Get timeframe data
    const { Planet, Satellite } = state.flyingObjectsFlightPatternLookup[
      timeframeSelected
    ];

    // Set active timeframe
    if (timeframeSelected) {
      dispatch({
        type: SET_DASHBOARD_ACTIVE_TIMESTAMP,
        activeTimestamp: timeframeSelected
      });
    }

    // Set Planet Data
    const planetFlightSummary = Planet;
    if (planetFlightSummary) {
      dispatch({
        type: SET_DASHBOARD_PLANET_FLIGHT_SUMMARY,
        planetFlightSummary
      });
    }

    // Set Sat. Data
    const satelliteFlightSummary = Satellite;
    if (satelliteFlightSummary) {
      dispatch({
        type: SET_DASHBOARD_SAT_FLIGHT_SUMMARY,
        satelliteFlightSummary
      });
    }
  };

  const playTimeline = () => {
    const currentTimelineIndex = state.flyingObjectTimeframes.indexOf(
      state.activeTimestamp.toString()
    );
    const nextTimeframe =
      state.flyingObjectTimeframes[currentTimelineIndex + 1];
    setTimeout(() => {
      setTimeframe({ target: { value: nextTimeframe } });
      generateDashboardDataSets();
    }, 500);
  };

  const setSliderTimeframe = event => {
    setTimeframe(event);
    if (state.isPlaying) {
      togglePlayer();
      clearTimeout(playTimeline);
    }
  };

  // Generate Dashboard Data sets
  React.useEffect(() => {
    if (state.isPlaying) {
      playTimeline();
    }
    generateDashboardDataSets();
  }, [state.isPlaying, state.activeTimestamp]);

  React.useEffect(() => {
    generateDashboardDataSets();
  }, []);

  const memoizedValue = React.useMemo(() => {
    return {
      flyingObjectsPlotData: state.flyingObjectsPlotData,
      flyingObjectSummaryData: state.flyingObjectSummaryData,
      flyingObjectTimeframes: state.flyingObjectTimeframes,
      satelliteFlightSummary: state.satelliteFlightSummary,
      planetFlightSummary: state.planetFlightSummary,
      activeTimestamp: state.activeTimestamp,
      setTimeframe: setSliderTimeframe,
      togglePlayer: togglePlayer,
      isPlaying: state.isPlaying
    };
  }, [state]);

  return (
    <SedaroDashboardContext.Provider value={memoizedValue}>
      {children}
    </SedaroDashboardContext.Provider>
  );
};

// Use Survey State Context
const useSedaroDashboardContext = () => {
  const context = React.useContext(SedaroDashboardContext);
  if (context === undefined) {
    throw new Error(
      "useSedaroDashboardContext must be used within a SedaroDashboardProvider"
    );
  }
  return context;
};

SedaroDashboardProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node)
  ]).isRequired
};

export { SedaroDashboardProvider, useSedaroDashboardContext };
