import React from "react";
import FlyingObjectsPlot from "./FlyingObjectsPlot";
import AppHeader from "./AppHeader";
import { makeStyles } from "@mui/styles";
import { Box, Theme, Typography } from "@mui/material";
import DashboardCard from "./DashboardCard";
import PlanetFlightDetail from "./PlanetFlightDetail";
import SatelliteFlightDetail from "./SatelliteFlightDetail";
import { ChatRounded, Public, Satellite } from "@material-ui/icons";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    height: "100vh",
    backgroundColor: "black",
    padding: 20
  },
  cardContainer: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    alignItems: "stretch"
  },
  detailContainer: {
    flexGrow: 1,
    height: "100%",
    marginLeft: "20px"
  }
}));

const Dashboard = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Typography
        variant="h5"
        sx={{
          fontWeight: 700,
          flexShrink: 0
        }}
      >
        {"Sedaro-Nano"}
      </Typography>
      <AppHeader />
      <Box className={classes.cardContainer}>
        <DashboardCard title="Flight Pattern" icon={<ChatRounded />}>
          <FlyingObjectsPlot />
        </DashboardCard>
        <Box className={classes.detailContainer}>
          <DashboardCard title="Satellite" icon={<Satellite />}>
            <SatelliteFlightDetail />
          </DashboardCard>
        </Box>
        <Box className={classes.detailContainer}>
          <DashboardCard title="Planet" icon={<Public />}>
            <PlanetFlightDetail />
          </DashboardCard>
        </Box>
      </Box>
    </div>
  );
};

export default Dashboard;
