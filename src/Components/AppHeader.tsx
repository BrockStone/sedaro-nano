import {
  Box,
  IconButton,
  Slider,
  Theme,
  Typography,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import React from "react";
import { useSedaroDashboardContext } from "../Contexts/SedaroDashboard";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import PauseIcon from "@material-ui/icons/Pause";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#101011",
    padding: 20,
    marginBottom: 20,
  },
  player: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
  },
  slider: {
    width: "100%",
    marginLeft: 20,
  },
}));

const AppHeader = () => {
  const classes = useStyles();
  const {
    flyingObjectTimeframes,
    setTimeframe,
    togglePlayer,
    isPlaying,
    activeTimestamp,
  } = useSedaroDashboardContext();

  // Convert timeframes
  const getTimeframes = () => {
    const timeframes = flyingObjectTimeframes as string[];
    return timeframes?.map((item) => ({
      value: Number(item),
    }));
  };

  return (
    <div className={classes.root}>
      <Box className={classes.player}>
        <Box>
          <IconButton
            aria-label="play"
            onClick={togglePlayer}
            sx={{
              backgroundColor: "#1976d2",
              color: "black",
              fontWeight: 700,
              "&:hover": {
                backgroundColor: "#1976d2",
                color: "black",
              },
            }}
          >
            {isPlaying ? <PauseIcon /> : <PlayArrowIcon />}
          </IconButton>
        </Box>
        <Typography sx={{ marginLeft: "20px" }}>{activeTimestamp}</Typography>
        <Box className={classes.slider}>
          <Slider
            marks={getTimeframes()}
            onChange={setTimeframe}
            step={null}
            max={22}
            value={activeTimestamp ? Number(activeTimestamp) : 0}
          />
        </Box>
      </Box>
    </div>
  );
};

export default AppHeader;
