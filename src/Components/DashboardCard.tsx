import { Box, Theme, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React, { ReactNode } from "react";

type DashboardCardProps = {
  children: ReactNode;
  title: string;
  icon: ReactNode;
};

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    backgroundColor: "#101011",
    padding: 20,
  },
}));

const DashboardCard = ({ children, title, icon }: DashboardCardProps) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          marginBottom: "20px",
        }}
      >
        {icon ? icon : null}
        <Typography
          variant="h5"
          sx={{
            marginLeft: "10px",
            fontWeight: 700,
            flexShrink: 0,
          }}
        >
          {title}
        </Typography>
      </Box>
      {children}
    </div>
  );
};

export default DashboardCard;
