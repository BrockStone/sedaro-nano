import React from "react";
import { makeStyles } from "@mui/styles";
import { useSedaroDashboardContext } from "../Contexts/SedaroDashboard";
import DetailSet from "./DetailSet";
import { Box, Theme } from "@material-ui/core";
import { Typography } from "@mui/material";

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  detailSet: {},
}));

const PlanetFlightDetail = () => {
  const { planetFlightSummary } = useSedaroDashboardContext();
  const classes = useStyles();
  return (
    <Box className={classes.detailSet}>
      {planetFlightSummary ? (
        <DetailSet
          description="Current planet position details."
          items={[
            {
              label: "Current Horizontal Position",
              value: planetFlightSummary["x"][0],
            },
            {
              label: "Current Vertical Position",
              value: planetFlightSummary["y"][0],
            },
            {
              label: "Current Vx",
              value: planetFlightSummary["vx"][0],
            },
            {
              label: "Current Vy",
              value: planetFlightSummary["vy"][0],
            },
          ]}
        />
      ) : (
        <Typography>
          {"Planet data will appear once simulation is started."}
        </Typography>
      )}
    </Box>
  );
};

export default PlanetFlightDetail;
