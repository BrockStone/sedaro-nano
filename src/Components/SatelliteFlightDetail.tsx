import React from "react";
import { makeStyles } from "@mui/styles";
import { useSedaroDashboardContext } from "../Contexts/SedaroDashboard";
import DetailSet from "./DetailSet";
import { Box, Theme } from "@material-ui/core";
import { Typography } from "@mui/material";

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  detailSet: {},
}));

const SatelliteFlightDetail = () => {
  const { satelliteFlightSummary } = useSedaroDashboardContext();
  const classes = useStyles();
  return (
    <Box className={classes.detailSet}>
      {satelliteFlightSummary ? (
        <DetailSet
          description="Current satellite position details."
          items={[
            {
              label: "Current Horizontal Position",
              value: satelliteFlightSummary["x"][0],
            },
            {
              label: "Current Vertical Position",
              value: satelliteFlightSummary["y"][0],
            },
            {
              label: "Current Vx",
              value: satelliteFlightSummary["vx"][0],
            },
            {
              label: "Current Vy",
              value: satelliteFlightSummary["vy"][0],
            },
          ]}
        />
      ) : (
        <Typography>
          {"Satellite data will appear once simulation is started."}
        </Typography>
      )}
    </Box>
  );
};

export default SatelliteFlightDetail;
