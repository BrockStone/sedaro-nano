import { Box, Theme, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React from "react";

type itemObj = {
  label: string;
  value: string;
};

type DetailSetProps = {
  items: itemObj[];
  description: string;
};

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  items: {},
  item: {
    borderTop: "1px solid #343337",
    paddingTop: 10,
    paddingBottom: 10,
  },
  itemCount: {
    fontSize: 20,
  },
  itemLabel: {
    fontSize: "0.8em",
  },
}));

const DetailSet = ({ items, description }: DetailSetProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography sx={{ fontSize: "0.9em", fontWeight: 400 }}>
        {description}
      </Typography>
      <Box className={classes.items}>
        {items.map((item) => {
          return (
            <Box key={item.value + item.label} className={classes.item}>
              <Typography sx={{ fontSize: "1.2em", fontWeight: 700 }}>
                {item.value}
              </Typography>
              <Typography sx={{ fontSize: "0.9em", fontWeight: 400 }}>
                {item.label}
              </Typography>
            </Box>
          );
        })}
      </Box>
    </div>
  );
};

export default DetailSet;
