import React from "react";
import Plotly from "plotly.js-dist-min";
import createPlotlyComponent from "react-plotly.js/factory";
import { useSedaroDashboardContext } from "../Contexts/SedaroDashboard";

const FlyingObjectVxPlot = () => {
  const {
    satelliteFlightSummary,
    planetFlightSummary,
    flyingObjectVxSummaryPlotData,
  } = useSedaroDashboardContext();
  const Plot = createPlotlyComponent(Plotly);
  const layout = {
    width: 300,
    xaxis: {
      range: [-1.5, 1.5],
      showgrid: false,
    },
    yaxis: {
      range: [0, 24],
      showgrid: false,
    },
    legend: {
      orientation: "h",
      x: 0.5,
      y: 1.2,
      xanchor: "center",
    },
    font: {
      color: "#fff",
      family:
        "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen','Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif",
    },
    plot_bgcolor: "#101011",
    paper_bgcolor: "#101011",
  };

  return <Plot data={flyingObjectVxSummaryPlotData} layout={layout} />;
};

export default FlyingObjectVxPlot;
