import React from "react";
import Plotly from "plotly.js-dist-min";
import createPlotlyComponent from "react-plotly.js/factory";
import { useSedaroDashboardContext } from "../Contexts/SedaroDashboard";

const FlyingObjectsPlot = () => {
  const { flyingObjectsPlotData } = useSedaroDashboardContext();
  const Plot = createPlotlyComponent(Plotly);
  const layout = {
    font: {
      color: "#fff",
      family:
        "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen','Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif"
    },
    xaxis: {
      showgrid: false
    },
    yaxis: {
      // range: [-2, 2],
      showgrid: false
    },
    plot_bgcolor: "#101011",
    paper_bgcolor: "#101011"
  };

  return <Plot data={flyingObjectsPlotData} layout={layout} />;
};

export default FlyingObjectsPlot;
